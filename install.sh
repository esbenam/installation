#!/bin/sh

#
#
# Install Homebrew and cask (runs `xcode-select --install` if needed - CommandlineTools)
#
#

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew doctor
brew install cask
brew doctor

#
#
# Install needed applications via Homebrew
#
#

# Terminal
brew cask install iterm2


# Utilities
brew cask install 1password
brew cask install cleanmymac
brew cask install caffeine
brew cask install divvy

# # Virtual machines and docker
# brew install parallels
# brew install docker
# brew install docker-compose

# Browsers
brew cask install google-chrome
brew cask install firefox
brew cask install microsoft-edge

# Graphics
brew cask install sketch
brew cask install abstract
brew cask install imageoptim

# General applications
brew cask install dropbox
brew cask install spotify
brew cask install slack

#
#
# Nodejs
#
# nvm doesn't officially support installation via homebrew
# so we use the official installation instructions from the
# repository.
#
#

/bin/bash -c "$(curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh)"

# make sure the just installed nvm is loaded
source ~/.zshrc

nvm install stable
nvm alias default stable

#
#
# doom emacs (https://github.com/hlissner/doom-emacs/blob/develop/docs/getting_started.org#on-macos)
#
#

# Required dependencies
brew install git
brew install ripgrep

# Optional dependencies
brew install fontconfig
brew install coreutils
brew install fd

# Recommmended way of installing emacs for doom
brew tap d12frosted/emacs-plus
brew install emacs-plus
ln -s /usr/local/opt/emacs-plus/Emacs.app /Applications/Emacs.app

# Clone and install doom
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install
