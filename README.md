# Installation script for a clean mac

Run this command from your terminal to install

```bash
curl -o- https://bitbucket.org/esbenam/installation/raw/master/install.sh | bash
```

Run this command from your terminal to uninstall (work in progress)

```bash
curl -o- https://bitbucket.org/esbenam/installation/raw/master/uninstall.sh | bash
```
